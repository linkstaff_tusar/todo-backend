const express = require('express');
const app = express();
const bodyParser =require('body-parser');
const cors = require('cors');
const mongoose = require('mongoose');

const Connection = mongoose.connection;
const todoRoutes = express.Router();
let Todo = require('./todo.model');

const PORT = 4000;

mongoose.connect('mongodb://127.0.0.1:27017/todos', {useNewUrlParser:true});
Connection.once('open',function(){
    console.log("MongoDB Connected");
});


todoRoutes.route('/').get(function(req,res){
    Todo.find(function(err,todos){
        if(err){
            console.log('This is an err0r->'+err);
        }else{
            res.json(todos);
        }
    });
});

todoRoutes.route('/:id').get(function(req,res){
    let id = req.params.id;
    Todo.findById(id,function(err,todo){
        res.json(todo);
    });
});
todoRoutes.route('/add').post(function(req,res){
    let todo = new Todo(req.body);
    todo.save()
    .then(todo=>{
        res.status(200).json({'todo':'todo added'});
    })
    .catch(err=>{
        res.status(400).json('adding failed');
    });
});

todoRoutes.route('/update/:id').post(function (req,res) {
    Todo.findById(req.params.id,function(err,todo){
        if(!todo){
            res.status(404).send('data is not found');
            console.log('This is an error');
        } else{
            todo.todo_desc = req.body.todo_desc;
            todo.todo_resp = req.body.todo_resp;
            todo.todo_completed = req.body.todo_completed;

            todo.save().then(todo => {
                res.status(200).json('Database Updated');
            }).catch(err=>{
                res.status(400).send('Update not Possible');
            });
        }
    });
});


app.use(cors());
app.use(bodyParser.json());
app.use('/todos',todoRoutes);
app.listen(PORT,function(){
    console.log("Server is connected at port--> "+PORT);
});